Rails.application.routes.draw do
  get '/users', to: 'users#index'
  get 'users/show'
  devise_for :users, controllers: {
    sessions: 'users/sessions'
  }
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
